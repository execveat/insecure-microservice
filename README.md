# Insecure Microservice 🔓

This microservice is insecure by design. It is meant to be used with my Medium blog, [GoLang 🐿 Application Security and Automation Made Easy](https://awkwardferny.medium.com/go-application-security-and-appsec-automation-made-easy-36bd2f3d520b). It's just a simple web-server that just prints meow and your name, and can show a random error. See the functionality below:

```bash
$ curl -X GET 0.0.0.0:8080/api/logic
{"message":"Meowwwww"}

$ curl -X POST 0.0.0.0:8080/api/logic -d '{"name": "Joe"}'
{"message":"Meowwwww, Joe"}

$ curl -X GET 0.0.0.0:8080/api/insecure
{"message":"Error No. 0.6046602879796196"}
```

## Security Scans

This applications runs the following security scans:

* [GoSec](https://github.com/securego/gosec)
* [Go-Vuln-Check](https://go.dev/blog/vuln)
* [Fuzz](https://go.dev/security/fuzz/)

Note: [Unit Tests](https://pkg.go.dev/testing) are also run.

## Insecure Code

The insecure code can be seen in **internal/logic/logic.go**. The types vulnerabilities we can see
are as follows:

* Uncaught exceptions
* Insecure libraries/dependencies
* Test failures via fuzzing

Look at the Medium blog, [GoLang 🐿 Application Security and Automation Made Easy](https://awkwardferny.medium.com/go-application-security-and-appsec-automation-made-easy-36bd2f3d520b) to learn more about these vulnerabilities.

## Running Locally

To run locally, you must be running Go 19.0+

### Application

1. Set the GOPATH and go to it

```bash
$ export GOPATH=/path/to/go/projects
```

2. Make sure to add your Go bin path to your path.

```bash
$ export PATH=$PATH:$GOPATH/bin
```

3. Create directory and clone this application

```bash
# Go into your GOPATH
$ cd $GOPATH

# Create GOPATH directory for this application
$ mkdir -p src/gitlab.com/awkwardferny

# Clone the application into the GOPATH
$ git clone git@gitlab.com:awkwardferny/insecure-microservice.git src/gitlab.com/awkwardferny/insecure-microservice

# Go into the application root
$ cd src/gitlab.com/awkwardferny/insecure-microservice
```

4. Edit Makefile to include your $GO, $GOOS, and $GOARCH environment variables

```text
GO = go
GOOS = darwin
GOARCH = arm64
```

Note: I am running on an M1 mac. If you are not, you will need to change
these items.

5. Build the executable

```bash
$ make build

go mod download
GOOS=darwin GOARCH=arm64 go build -o insecure-microservice .
chmod +x insecure-microservice
```

6. Run the executable

```bash
./insecure-microservice

2022/09/20 14:16:30 Starting server on the port 8080
```

**Note:** You can set the `INSECURE_PORT` environment variable before running
the application to change the port it will run on.

7. Open a separate terminal and send a request

```bash
$ curl 0.0.0.0:8080/api/logic

{"message":"Meowwwww"}
```

### Tests

1. Complete Steps 1-3 in the Application section of Running Locally seen above.

2. Run Unit tests

```bash
$ make test

go test -v ./...
?       gitlab.com/awkwardferny/insecure-microservice   [no test files]
=== RUN   TestMeow
--- PASS: TestMeow (0.00s)
=== RUN   FuzzAdd
=== RUN   FuzzAdd/seed#0
--- PASS: FuzzAdd (0.00s)
    --- PASS: FuzzAdd/seed#0 (0.00s)
PASS
ok      gitlab.com/awkwardferny/insecure-microservice/internal/logic    (cached)
?       gitlab.com/awkwardferny/insecure-microservice/internal/router   [no test files]
```

3. Run GoSec

```bash
$ make gosec

go install github.com/securego/gosec/v2/cmd/gosec@latest
gosec ./...
[gosec] 2022/09/21 15:32:53 Including rules: default
[gosec] 2022/09/21 15:32:53 Excluding rules: default
[gosec] 2022/09/21 15:32:53 Import directory: /Users/fern/Documents/Development/Go/src/gitlab.com/awkwardferny/insecure-microservice
[gosec] 2022/09/21 15:32:53 Import directory: /Users/fern/Documents/Development/Go/src/gitlab.com/awkwardferny/insecure-microservice/internal/router
[gosec] 2022/09/21 15:32:53 Import directory: /Users/fern/Documents/Development/Go/src/gitlab.com/awkwardferny/insecure-microservice/internal/logic
[gosec] 2022/09/21 15:32:53 Import directory: /Users/fern/Documents/Development/Go/src/gitlab.com/awkwardferny/insecure-microservice/internal/model
[gosec] 2022/09/21 15:32:53 Checking package: model
[gosec] 2022/09/21 15:32:53 Checking file: /Users/fern/Documents/Development/Go/src/gitlab.com/awkwardferny/insecure-microservice/internal/model/model.go
[gosec] 2022/09/21 15:32:54 Checking package: logic
[gosec] 2022/09/21 15:32:54 Checking file: /Users/fern/Documents/Development/Go/src/gitlab.com/awkwardferny/insecure-microservice/internal/logic/logic.go
[gosec] 2022/09/21 15:32:54 Checking package: router
[gosec] 2022/09/21 15:32:54 Checking file: /Users/fern/Documents/Development/Go/src/gitlab.com/awkwardferny/insecure-microservice/internal/router/router.go
[gosec] 2022/09/21 15:32:54 Checking package: main
[gosec] 2022/09/21 15:32:54 Checking file: /Users/fern/Documents/Development/Go/src/gitlab.com/awkwardferny/insecure-microservice/main.go
Results:


[/Users/fern/Documents/Development/Go/src/gitlab.com/awkwardferny/insecure-microservice/internal/logic/logic.go:67] - G404 (CWE-338): Use of weak random number generator (math/rand instead of crypto/rand) (Confidence: MEDIUM, Severity: HIGH)
    66:                 // display a vulnerability
  > 67:                 return fmt.Sprintf("Error No. %v", rand.Float64())
    68:         }



[/Users/fern/Documents/Development/Go/src/gitlab.com/awkwardferny/insecure-microservice/main.go:27] - G114 (CWE): Use of net/http serve function that has no support for setting timeouts (Confidence: HIGH, Severity: MEDIUM)
    26:         log.Printf("Starting server on the port %s", port)
  > 27:         log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), r))
    28: 



[/Users/fern/Documents/Development/Go/src/gitlab.com/awkwardferny/insecure-microservice/internal/logic/logic.go:72] - G104 (CWE-703): Errors unhandled. (Confidence: HIGH, Severity: LOW)
    71:         // it as active. If not used, it will be shown as inactive
  > 72:         x.Read(a)
    73: 



[/Users/fern/Documents/Development/Go/src/gitlab.com/awkwardferny/insecure-microservice/internal/logic/logic.go:52] - G104 (CWE-703): Errors unhandled. (Confidence: HIGH, Severity: LOW)
    51:         message := map[string]string{"message": insecure()}
  > 52:         json.NewEncoder(w).Encode(message)
    53: }



[/Users/fern/Documents/Development/Go/src/gitlab.com/awkwardferny/insecure-microservice/internal/logic/logic.go:32] - G104 (CWE-703): Errors unhandled. (Confidence: HIGH, Severity: LOW)
    31:         message := map[string]string{"message": meow(meowRequest.Name)}
  > 32:         json.NewEncoder(w).Encode(message)
    33: }



[/Users/fern/Documents/Development/Go/src/gitlab.com/awkwardferny/insecure-microservice/internal/logic/logic.go:27] - G104 (CWE-703): Errors unhandled. (Confidence: HIGH, Severity: LOW)
    26:         if err != nil {
  > 27:                 json.NewEncoder(w).Encode(err.Error())
    28:                 return



Summary:
  Gosec  : dev
  Files  : 4
  Lines  : 142
  Nosec  : 0
  Issues : 6

make: *** [gosec] Error 1
```

4. Run GoVulnCheck

```bash
$ make govulncheck

go install golang.org/x/vuln/cmd/govulncheck@latest
go: downloading golang.org/x/vuln v0.0.0-20220921190058-4776cfabfb80
govulncheck ./...
govulncheck is an experimental tool. Share feedback at https://go.dev/s/govulncheck-feedback.

Scanning for dependencies with known vulnerabilities...
Found 1 known vulnerability.

Vulnerability #1: GO-2020-0016
  An attacker can construct a series of bytes such that calling
  Reader.Read on the bytes could cause an infinite loop. If
  parsing user supplied input, this may be used as a denial of
  service vector.

  Call stacks in your code:
      internal/logic/logic.go:72:8: gitlab.com/awkwardferny/insecure-microservice/internal/logic.insecure calls github.com/ulikunitz/xz.Reader.Read

  Found in: github.com/ulikunitz/xz@v0.5.7
  Fixed in: github.com/ulikunitz/xz@v0.5.8
  More info: https://pkg.go.dev/vuln/GO-2020-0016
make: *** [govulncheck] Error 3
```

5. Run Fuzz Test

```bash
$ make fuzz

go test ./internal/logic -fuzz FuzzAdd
fuzz: elapsed: 0s, gathering baseline coverage: 0/1 completed
fuzz: elapsed: 0s, gathering baseline coverage: 1/1 completed, now fuzzing with 10 workers
fuzz: minimizing 40-byte failing input file
fuzz: elapsed: 0s, minimizing
--- FAIL: FuzzAdd (0.10s)
    --- FAIL: FuzzAdd (0.00s)
        logic_test.go:44: expected 1, got 0
    
    Failing input written to testdata/fuzz/FuzzAdd/9f4dc959af0a73c061c4b4185e9fdb9e5dbfc854cccce7bf9199f0f5556c42a9
    To re-run:
    go test -run=FuzzAdd/9f4dc959af0a73c061c4b4185e9fdb9e5dbfc854cccce7bf9199f0f5556c42a9
FAIL
exit status 1
FAIL    gitlab.com/awkwardferny/insecure-microservice/internal/logic    0.448s
make: *** [fuzz] Error 1
```

---

Thanks for using! If you enjoyed, tweet about it and tag me [@awkwardferny](https://twitter.com/awkwardferny) on it.

Photo by [おにぎり](https://unsplash.com/@fukayamamo?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">おにぎり) on [Unsplash](https://unsplash.com/s/photos/pod?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
