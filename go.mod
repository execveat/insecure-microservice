module gitlab.com/awkwardferny/insecure-microservice

go 1.18

require github.com/gorilla/mux v1.8.0

require github.com/ulikunitz/xz v0.5.7
