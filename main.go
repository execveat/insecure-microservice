package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"

	"gitlab.com/awkwardferny/insecure-microservice/internal/router"
)

func main() {
	// Get port from env, or set to 8080 by default
	port := os.Getenv("INSECURE_PORT")
	if port == "" {
		port = "8080"
	}

	_, err := strconv.Atoi(port)
	if err != nil {
		log.Fatal(`"INSECURE_PORT" must be an integer`)
	}

	r := router.Router()
	log.Printf("Starting server on the port %s", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), r))

}
