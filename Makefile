GO = go
GOOS = darwin
GOARCH = arm64

build:
	${GO} mod download
	GOOS=${GOOS} GOARCH=${GOARCH} ${GO} build -o insecure-microservice .
	chmod +x insecure-microservice

test:
	${GO} test -v ./...

gosec:
	${GO} install github.com/securego/gosec/v2/cmd/gosec@latest
	gosec ./...

govulncheck:
	${GO} install golang.org/x/vuln/cmd/govulncheck@latest
	govulncheck ./...

fuzz:
	${GO} test ./internal/logic -fuzz FuzzAdd

clean:
	rm -rf insecure-microservice
	rm -rf internal/logic/testdata