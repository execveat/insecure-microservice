package model

// Data to grab from request to Meow
type MeowRequestData struct {
	Name string `json:"name"`
}
