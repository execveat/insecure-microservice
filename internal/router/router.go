package router

import (
	"gitlab.com/awkwardferny/insecure-microservice/internal/logic"

	"github.com/gorilla/mux"
)

func Router() *mux.Router {
	router := mux.NewRouter()

	router.HandleFunc("/api/logic", logic.GetMeow).Methods("GET", "POST", "OPTIONS")
	router.HandleFunc("/api/insecure", logic.GetInsecure).Methods("GET", "OPTIONS")

	return router
}
