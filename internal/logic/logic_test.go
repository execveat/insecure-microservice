package logic

import (
	"fmt"
	"strconv"
	"testing"
)

func TestMeow(t *testing.T) {
	name := ""
	result := meow(name)
	expected := Meow

	if result != expected {
		t.Errorf("expected '%s', got '%s'", expected, result)
	}

	name = "Joe"
	result = meow(name)
	expected = fmt.Sprintf("%s, %s", Meow, name)

	if result != expected {
		t.Errorf("expected '%s', got '%s'", expected, result)
	}

}

func FuzzAdd(f *testing.F) {
	f.Add("1", "2")
	f.Fuzz(func(t *testing.T, a string, b string) {
		// Run the function passing a, b as random/malformed values
		// from the fuzzer
		result, err := add(a, b)

		if err != nil {
			t.Errorf(fmt.Sprintf("error: %v", err))
		}

		intA, _ := strconv.Atoi(a)
		intB, _ := strconv.Atoi(b)
		expected := intA + intB

		if result != expected {
			t.Errorf(fmt.Sprintf("expected %v, got %v", expected, result))
		}
	})
}
