package logic

import (
	"bytes"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"strconv"

	"github.com/ulikunitz/xz"
	"gitlab.com/awkwardferny/insecure-microservice/internal/model"
)

var Meow = "Meowwwww"

// Create API to return Meow and supplied name
func GetMeow(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/x-www-form-urlencoded")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "OPTIONS, POST, GET")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type")

	var meowRequest model.MeowRequestData
	err := json.NewDecoder(r.Body).Decode(&meowRequest)
	if err != nil {
		json.NewEncoder(w).Encode(err.Error())
		return
	}

	message := map[string]string{"message": meow(meowRequest.Name)}
	json.NewEncoder(w).Encode(message)
}

// Return meow as well as user's name if it is present
func meow(name string) string {
	if name != "" {
		return fmt.Sprintf(Meow+", %s", name)
	}

	return Meow
}

// Returns random error message from insecure function
func GetInsecure(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/x-www-form-urlencoded")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "OPTIONS, GET")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type")

	message := map[string]string{"message": insecure()}
	json.NewEncoder(w).Encode(message)
}

func insecure() string {
	var a = []byte{0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87,
		0x88, 0x89, 0x8a, 0x8b}

	r := bytes.NewReader(a)
	x, err := xz.NewReader(r)

	// Return a random float as the Error number
	if err != nil {
		// Float64 is an insecure random algorithm if used in keys
		// but it's just being used here for output, still it will
		// display a vulnerability
		return fmt.Sprintf("Error No. %v", rand.Float64())
	}

	// Using this function causes the govulncheck check to report
	// it as active. If not used, it will be shown as inactive
	x.Read(a)

	return fmt.Sprintf("%v+", x)
}

// Converts strings to int and adds them
// it's just used to test fuzzing
func add(a string, b string) (c int, e error) {
	intA, err := strconv.Atoi(a)
	if err != nil {
		return 0, nil
	}

	intB, err := strconv.Atoi(b)
	if err != nil {
		return 0, nil
	}

	return (intA + intB), nil
}
